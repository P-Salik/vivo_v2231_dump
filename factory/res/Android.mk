LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

$(shell mkdir -p /factory/res)

audio_files := $(shell ls $(LOCAL_PATH))
PRODUCT_COPY_FILES += $(foreach file, $(audio_files), $(LOCAL_PATH)/$(file):factory/res/$(file))

include $(call first-makefiles-under,$(LOCAL_PATH))
