#!/bin/bash

cat system/system/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/priv-app/Settings/Settings.apk
rm -f system/system/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/priv-app/SystemUI/SystemUI.apk
rm -f system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v30.apex
rm -f system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system/system/framework/framework-res.apk.* 2>/dev/null >> system/system/framework/framework-res.apk
rm -f system/system/framework/framework-res.apk.* 2>/dev/null
cat system/system/framework/vivo-res.apk.* 2>/dev/null >> system/system/framework/vivo-res.apk
rm -f system/system/framework/vivo-res.apk.* 2>/dev/null
cat system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null >> system/system/app/VivoGallery/VivoGallery.apk
rm -f system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null
cat system/system/app/Gboard/Gboard.apk.* 2>/dev/null >> system/system/app/Gboard/Gboard.apk
rm -f system/system/app/Gboard/Gboard.apk.* 2>/dev/null
cat system/system/app/VivoThemeRes_T1/VivoThemeRes_T1.apk.* 2>/dev/null >> system/system/app/VivoThemeRes_T1/VivoThemeRes_T1.apk
rm -f system/system/app/VivoThemeRes_T1/VivoThemeRes_T1.apk.* 2>/dev/null
cat system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null >> system/system/app/VivoCamera/VivoCamera.apk
rm -f system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null
cat system/system/app/LiveWallpaperRes/LiveWallpaperRes.apk.* 2>/dev/null >> system/system/app/LiveWallpaperRes/LiveWallpaperRes.apk
rm -f system/system/app/LiveWallpaperRes/LiveWallpaperRes.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor/etc/nn/ot/neuron_detection.mdla3_5.* 2>/dev/null >> vendor/etc/nn/ot/neuron_detection.mdla3_5
rm -f vendor/etc/nn/ot/neuron_detection.mdla3_5.* 2>/dev/null
cat vendor/lib64/mt6886/libaibc_tuning.so.* 2>/dev/null >> vendor/lib64/mt6886/libaibc_tuning.so
rm -f vendor/lib64/mt6886/libaibc_tuning.so.* 2>/dev/null
cat vendor/lib64/mt6886/libaiawb_moon_model.so.* 2>/dev/null >> vendor/lib64/mt6886/libaiawb_moon_model.so
rm -f vendor/lib64/mt6886/libaiawb_moon_model.so.* 2>/dev/null
cat vendor/lib64/mt6886/libaibc_tuning_p2.so.* 2>/dev/null >> vendor/lib64/mt6886/libaibc_tuning_p2.so
rm -f vendor/lib64/mt6886/libaibc_tuning_p2.so.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat vendor_bootimg/00_kernel.* 2>/dev/null >> vendor_bootimg/00_kernel
rm -f vendor_bootimg/00_kernel.* 2>/dev/null
cat vendor_bootimg/01_dtbdump_MT6886.dtb.* 2>/dev/null >> vendor_bootimg/01_dtbdump_MT6886.dtb
rm -f vendor_bootimg/01_dtbdump_MT6886.dtb.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/app/Photos/Photos.apk.* 2>/dev/null >> product/app/Photos/Photos.apk
rm -f product/app/Photos/Photos.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
